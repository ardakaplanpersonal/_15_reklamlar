package com.androidegitim.reklamlar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;

    private RewardedVideoAd mRewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        RDALogger.start(getString(R.string.app_name)).enableLogging(true);

        loadInterstitialAd();

        loadRewardedAd();

        //uygulama kimliği
        MobileAds.initialize(this, "ca-app-pub-1752844286988411~9244761447");

        setBannerAd();
    }

    private void loadInterstitialAd() {

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1752844286988411/2099224000");

        final AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        adRequestBuilder.addTestDevice("D41FC4ABE337479189FBF37469236558");

        mInterstitialAd.loadAd(adRequestBuilder.build());

        mInterstitialAd.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                RDALogger.info("InterstitialAd onAdLoaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                RDALogger.info("InterstitialAd onAdFailedToLoad");
            }

            @Override
            public void onAdOpened() {
                RDALogger.info("InterstitialAd onAdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                RDALogger.info("InterstitialAd onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                mInterstitialAd.loadAd(adRequestBuilder.build());

                RDALogger.info("InterstitialAd onAdClosed");
            }
        });
    }

    @OnClick(R.id.main_button_full_screen_advert)
    public void openInterstitialAd() {

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();

        } else {

            RDALogger.info("interstitial hazır değil");
        }
    }

    private void loadRewardedAd() {

        final String adID = "ca-app-pub-1752844286988411/5777834312";

        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);

        final AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        adRequestBuilder.addTestDevice("D41FC4ABE337479189FBF37469236558");

        mRewardedVideoAd.loadAd(adID, adRequestBuilder.build());

        mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {

            @Override
            public void onRewardedVideoAdLoaded() {

                RDALogger.info("onRewardedVideoAdLoaded");
            }

            @Override
            public void onRewardedVideoAdOpened() {
                RDALogger.info("onRewardedVideoAdOpened");
            }

            @Override
            public void onRewardedVideoStarted() {
                RDALogger.info("onRewardedVideoStarted");
            }

            @Override
            public void onRewardedVideoAdClosed() {
                RDALogger.info("onRewardedVideoAdClosed");
                mRewardedVideoAd.loadAd(adID, adRequestBuilder.build());
            }

            @Override
            public void onRewarded(RewardItem rewardItem) {
                RDALogger.info("onRewarded");
            }

            @Override
            public void onRewardedVideoAdLeftApplication() {
                RDALogger.info("onRewardedVideoAdLeftApplication");
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {

                mRewardedVideoAd.loadAd(adID, adRequestBuilder.build());

                RDALogger.info("onRewardedVideoAdFailedToLoad");
            }
        });
    }

    @OnClick(R.id.main_button_rewarded_advert)
    public void openRewardedAd() {

        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        } else {

            RDALogger.info("RewardedVideoAd hazhır değil.");
        }
    }

    private void setBannerAd() {

        AdView mAdView = findViewById(R.id.adView);


        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        adRequestBuilder.addTestDevice("D41FC4ABE337479189FBF37469236558");

        AdRequest adRequest = adRequestBuilder.build();

        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {

                RDALogger.info("BANNER onAdLoaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

                RDALogger.info("BANNER onAdFailedToLoad");
            }

            @Override
            public void onAdOpened() {
                RDALogger.info("BANNER onAdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                RDALogger.info("BANNER onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                RDALogger.info("BANNER onAdClosed");
            }
        });
    }
}
